import firebase from "firebase/app";
import "firebase/performance";
import "firebase/auth";
import 'firebase/firestore'
import 'firebase/storage'

// firebase init goes here
const firebaseConfig = {
    apiKey: "AIzaSyB5Ebx3gJ_BpT94um9wzIUsgWhQrNL63wU",
    authDomain: "writer-dev-4efc9.firebaseapp.com",
    databaseURL: "https://writer-dev-4efc9.firebaseio.com",
    projectId: "writer-dev-4efc9",
    storageBucket: "writer-dev-4efc9.appspot.com",
    messagingSenderId: "699884004828",
    appId: "1:699884004828:web:1d79a23859f3e8ff429b19"
  };
firebase.initializeApp(firebaseConfig)


// Initialize Performance Monitoring and get a reference to the service
const perf = firebase.performance();

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

export {
    db,
    auth,
    firebaseConfig,
    currentUser
}