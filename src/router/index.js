import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase/app'
import Home from '../views/Home.vue'
import store from '@/store/index'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "auth" */ '../views/auth/Login.vue')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import(/* webpackChunkName: "auth" */ '../views/auth/Signup.vue')
  },
  {
    path: '/stories/:id',
    props: true,
    name: 'StoryDetail',
    component: () => import(/* webpackChunkName: "storyDetail" */ '../views/stories/StoryDetail.vue')
  },
  {
    path: '/feed',
    name: 'FeedIndex',
    component: () => import(/* webpackChunkName: "feedIndex" */ '../views/FeedIndex.vue')
  },
  {
    path: '/watch',
    name: 'WatchList',
    component: () => import(/* webpackChunkName: "watchList" */ '../views/WatchList.vue')
  },
  {
    path: '/communities',
    name: 'CommunityIndex',
    component: () => import(/* webpackChunkName: "CommunityIndex" */ '../views/communities/CommunityIndex.vue')
  },  
  {
    path: '/communities/new',
    name: 'CommunityNew',
    component: () => import(/* webpackChunkName: "communityNew" */ '../views/communities/CommunityNew.vue')
  },
  {
    path: '/communities/:slug/users',
    props: true,
    name: 'CommunitiesUsers',
    component: () => import(/* webpackChunkName: "communityUsers" */ '../views/communities/CommunityUsers.vue')
  },
  {
    path: '/communities/:slug',
    props: true,
    name: 'CommunitiesHome',
    component: () => import(/* webpackChunkName: "communityHome" */ '../views/communities/CommunityHome.vue')
  },
  {
    path: '/communities/:slug/posts',
    name: 'CommunityPostIndex',
    component: () => import(/* webpackChunkName: "CommunityPostIndex" */ '../views/communities/CommunityPostIndex.vue')
  },
  {
    path: '/users/:id',
    props: true,
    name: 'UsersProfile',
    component: () => import(/* webpackChunkName: "userProfile" */ '../views/UsersProfile.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    }
  }

})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
  const currentUser = firebase.auth().currentUser
  store.commit('SET_NAV', false)

  if (requiresAuth && !currentUser) {
    next('/login')
  } else if (requiresAuth && currentUser) {
    next()
  } else {
    next()
  }
})

export default router
