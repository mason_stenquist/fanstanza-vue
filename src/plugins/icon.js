import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


import { 
    faChevronUp, 
    faChevronDown, 
    faHeart, 
    faShareAlt, 
    faBell, 
    faBook, 
    faCog, 
    faPen,
    faPlus,
    faBars,
    faLock,
    faUser,
    faSignOutAlt,
    faTimes
} from '@fortawesome/free-solid-svg-icons'
 
library.add(faChevronUp)
library.add(faChevronDown)
library.add(faHeart)
library.add(faShareAlt)
library.add(faBell)
library.add(faBook)
library.add(faCog)
library.add(faPen)
library.add(faPlus)
library.add(faBars)
library.add(faLock)
library.add(faUser)
library.add(faSignOutAlt)
library.add(faTimes)



 
Vue.component('font-awesome-icon', FontAwesomeIcon)