import Vue from 'vue'
import Vuex from 'vuex'
import {auth, db} from '@/firebase'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

// handle page reload
auth.onAuthStateChanged(user => {
  if (user) {
      store.commit('SET_CURRENT_USER', user)
      store.dispatch('fetchUserProfile')

      db.collection('users').doc(user.uid).onSnapshot(doc => {
          store.commit('SET_USER_PROFILE', doc.data())
      })
  }
})

const store = new Vuex.Store({
  modules: {
    
  },
  state: {
    currentUser: null,
    userProfile: {},
    navOpen: false
  },
  actions: {
    clearData({ commit }) {
        commit('SET_CURRENT_USER', null)
        commit('SET_USER_PROFILE', {})
    },
    fetchUserProfile({ commit, state }) {
        db.collection('users').doc(state.currentUser.uid).get().then(res => {
          console.log(res)
          console.log(res.data())
            commit('SET_USER_PROFILE', res.data())
        }).catch(err => {
            console.log(err)
        })
    }
  },
  mutations: {
    SET_CURRENT_USER(state, val) {
        state.currentUser = val
    },
    SET_USER_PROFILE(state, val) {
        state.userProfile = val
    },
    SET_NAV(state, val){
      state.navOpen = val
    }
  },
  plugins: [createPersistedState({
    paths: []
  })]
})

export default store
